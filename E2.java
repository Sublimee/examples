public class E2 {

    static int a = 5;
    static Integer b = 5;

    public static void changeValue(Integer value /*ссылка со значением 5*/) {
        value = 6; /* другая ссылка, которая смотрит на другой объект */
    }

    static public int change(int a) {
        return a++;
    }

    public static void changeClassValue(Test value) {
        value.value = 10;
    }

    public static void doNotChangeClassValue(Test value) {
        value = new Test();
        value.value = 10;
    }

    public static void main(String[] args) {
        int c = 5;
        c = change(c);
        System.out.println(c); // 5
        changeValue(c);
        System.out.println(c); // 5

        System.out.println("---------");

        System.out.println(a); // 5
        changeValue(a);
        System.out.println(a); // 5

        System.out.println("---------");

        System.out.println(b); // 5
        changeValue(b);
        System.out.println(b); // 5

        System.out.println("---------");

        Test testObject = new Test();
        System.out.println(testObject.value); // 6
        doNotChangeClassValue(testObject);
        System.out.println(testObject.value); // 6
        changeClassValue(testObject);
        System.out.println(testObject.value); // 10

    }

    static class Test {
        Integer value = 6;
    }

}


