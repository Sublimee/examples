import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// после этого посмотреть E13
public class E4 {

    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        C c = new C();
        D d = new D();

        /* PECS */

        /*  "Producer Extends"
            If you need a List to produce T values (you want to read Ts from the list), you need to
            declare it with ? extends T, e.g. List<? extends Integer>. But you cannot add to this list
        */
        List<? extends B> listExtends;
        // listExtends во время исполнения программы (в рантайме) может указывать на один из следующих типов:
        // List<B>, List<C>, List<D>, но на какой точно в рантайме - неизвестно, ...
        listExtends = new ArrayList<Object>(); // Object not extends B
        listExtends = new ArrayList<A>(); // A not extends B
        listExtends = new ArrayList<B>(); // аналогично listExtends = new ArrayList<>(); B в <> можно не указывать
        listExtends = new ArrayList<C>();
        listExtends = new ArrayList<D>();
        // ... поэтому у нас нет возможности положить объект в такой лист:
        listExtends.add(new Object()); // Object not extends B
        listExtends.add(a); // A not extends B
        listExtends.add(b); // а вдруг listExtends указывает на new ArrayList<С>()?
        listExtends.add(c); // а вдруг listExtends указывает на new ArrayList<D>()?
        listExtends.add(d); // а вдруг listExtends указывает на new ArrayList<B>()?

        // мы не знаем, на какой точно тип (List<B>, List<C>, List<D>) указывает
        // переменная, но знаем, что это переменная не ниже в иерархии, чем B, поэтому
        listExtends.forEach(x -> x.printA()); // ok
        listExtends.forEach(x -> x.printB()); // ok
        // а про эти методы компилятор уже ничего не знает
        listExtends.forEach(x -> x.printC());
        listExtends.forEach(x -> x.printD());

        // Может показаться, что есть противоречие:
        List<? extends B> foo_extends_4 = Arrays.asList(new B(), new C(), new D()); // значение справа имеет тип List<B>
        // Однако в этот момент у компилятора есть информация о типах и такое он сделать не даст:
        foo_extends_4 = Arrays.asList(new A(), new B(), new C(), new D()); // значение справа имеет тип List<A>


        /* "Consumer Super"
            If you need a List to consume T values (you want to write Ts into the list), you need to declare it
            with ? super T, e.g. List<? super Integer>. But there are no guarantees what type of object you may read from this list
         */
        List<? super B> listSuper;
        // listSuper во время исполнения программы (в рантайме) может указывать на один из следующих типов:
        listSuper = new ArrayList<Object>();
        listSuper = new ArrayList<A>(); // A not extends B
        listSuper = new ArrayList<B>(); // аналогично listSuper = new ArrayList<>(); "B" в <> можно не указывать
        listSuper = new ArrayList<C>(); // С является подтипом B, не является для B суперклассом
        listSuper = new ArrayList<D>();// D является подтипом B, не является для B суперклассом

        // мы не знаем, на какой точно тип (List<Object>, List<A>, List<B>) указывает
        // переменная, но знаем, что это переменная не выше в иерархии, чем B, поэтому
        listSuper.add(new Object()); // // а вдруг listSuper указывает на new ArrayList<A>()? а мы пытаемся положить Object
        listSuper.add(a); // // а вдруг listSuper указывает на new ArrayList<B>()? а мы пытаемся положить A
        listSuper.add(b); // b может заменить любой из своих суперклассов (Object, A), он знает все об наследуемых методах и полях
        listSuper.add(c); // с может заменить любой из своих суперклассов (Object, A, B), он знает все об наследуемых методах и полях
        listSuper.add(d); // в может заменить любой из своих суперклассов (Object, A, B, C), он знает все об наследуемых методах и полях

        // так как в рантайме мы не знаем, на какой точно тип (List<Object>, List<A>, List<B>) указывает
        // переменная, то и обращаться к каким-либо методам этих классов A, B, C, D мы не можем, но доступны методы Object
        listSuper.forEach(x -> x.printA());
        listSuper.forEach(x -> x.printB());
        listSuper.forEach(x -> x.printC());
        listSuper.forEach(x -> x.printD());
        listSuper.forEach(x -> x.toString());
    }

}

class A {
    public void printA() {
    }
}

class B extends A {
    public void printB() {
    }
}

class C extends B {
    public void printC() {
    }
}

class D extends C {
    public void printD() {
    }

}
