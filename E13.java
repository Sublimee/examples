import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class E13 {

    public static void main(String[] args) {
        // List в рантайме указывает на коллекцию с одним определенным типом, на какой конкретно на этапе компиляции
        // мы узнать не можем
        List<?> list = new ArrayList<>();
        // поэтому положить элемент в такой лист нельзя
        list.add(5);
        // Это как super, но без возможности положить туда объект

        // Рассмотрим передачу параметров:
        analyze(list);
        analyzeSafe(list);
    }

    public static void analyze(List list){
        list.add(new Object()); // здесь будет ошибка в рантайме
        System.out.println("it's ok");
    }

    public static void analyzeSafe(List<?> list){
        list.add(new Object()); // здесь ошибка компиляции, что, несомненно, лучше
        System.out.println("it's ok");
    }

}
