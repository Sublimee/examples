public class E3 {


    public static void main(String[] args) {
        A a0 = new A();
        A a1 = new B();

        System.out.println(a0.a);  // 5
        System.out.println(a1.a);  // 5

        a0.print(); // 5
        a1.print(); // 55
    }

    static class A {
        int a = 5;

        public void print() {
            System.out.println(a);
        }
    }

    static class B extends A {
        int a = 55;

        public void print() {
            System.out.println(a);
        }
    }

}
