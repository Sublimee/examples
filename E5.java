import java.util.*;

public class E5 {

    public static void main(String[] args) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(20, 0);
        map.put(30, 0);

        for (Integer key : map.keySet()) {
            System.out.println(key);
            map.put(40, 0);
        }

        map.keySet().forEach(key -> {
            System.out.println(key);
            map.put(40, 0);
        });
    }
}
