public class E12 {

    private E12() {
    }

    public static final String IS_TEST = "isTest";

    static boolean areEquals(String test) {
        return IS_TEST.equalsIgnoreCase(test);
    }

}
