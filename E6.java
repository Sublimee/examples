import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class E6 {

    public static void main(String[] args) {
        Map<Integer, Integer> map = new ConcurrentHashMap<>();
        map.put(20, 0);
        map.put(30, 0);

        for (Integer key : map.keySet()) {
            System.out.println(key);
            map.put(40, 0);
        }

        map.keySet().forEach(key -> {
            System.out.println(key);
            map.put(40, 0);
        });
    }

}
