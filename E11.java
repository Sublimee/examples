import javax.security.auth.login.AccountException;

public class E11 {

    public static void main(String[] args) throws AccountException {
        int value = withdrawSumFromAccount(50, 10);
    }

    private static String extracted() {
        throw new NullPointerException();
    }

    static public int withdrawSumFromAccount(int sum, int accountAmount) throws AccountException {
        if (sum > 0 && accountAmount > 0 && accountAmount >= sum) {
            return accountAmount - sum;
        }
        throw new AccountException(
                String.format("withdrawSumFromAccount failed with parameters: sum=%s, accountAmount=%s", sum, accountAmount));
    }

}
