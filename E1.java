public class E1 {

    public static void main(String[] args) {
        int a = 500;
        int b = 500;
        System.out.println(a == b); // true

        Integer a1 = 500;
        Integer b1 = 500;
        System.out.println(a1 == b1); // false

        Integer a2 = 100;
        Integer b2 = 100;
        System.out.println(a2 == b2); // true

        Integer a3 = new Integer(50);
        Integer b3 = 50;
        System.out.println(a3 == b3); // false
    }

}
